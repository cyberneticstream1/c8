"use client";
import React from "react";
import PrimaryText from "../components/PrimaryText";
import {ThemeContext} from "../components/ThemeContext";
import Link from "next/link";
export default function Page(){

    const [ua] = React.useState(React.useContext(ThemeContext))
    const [mobileVisibility, setMobileVisibility] = React.useState("hidden")
    const [pCVisibility, setPCVisibility] = React.useState("hidden")

    React.useEffect(() => {
        if (ua.isMobile) {
            setMobileVisibility("block")
            setPCVisibility("hidden")
        } else{
            setMobileVisibility("hidden")
            setPCVisibility("block")
        }
    },[])

        return(
                <>

                <PrimaryText childrenMenu={
                    <>
                    <Link href="/" alt={"10144"}>10144 boca entrada</Link> <br/>
                    <Link href="/autopay" alt={"autopay"}>autopay</Link> <br/>
                    <Link href="/dec22" alt={"dec22"}>dec22</Link> <br/>
                    <Link href="/log" alt={"log"}>...</Link> <br/>
                    </>
                }/>


                </>
                )

}