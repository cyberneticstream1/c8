"use client"
import Link from "next/link";
import PrimaryText from "../../components/PrimaryText";
import {createTheme, ThemeProvider} from "@mui/material/styles";
import Stack from "@mui/material/Stack";
import {Button} from "@mui/material";
import {grey} from "@mui/material/colors";



const theme = createTheme({
    palette: {
        primary: {
            main: grey["A100"],
        },
        secondary: {
            main: '#11cb5f',
        },
    },
    typography: {
        fontFamily: "var(--font-inter)",
    }

});

export async function generateStaticParams(){
    return [{id: "dec22"}]
}

export default function Page({params}){
    let id = params.id

    return(
            <>

            <PrimaryText childrenMenu={
                <>

                <Link href="/" alt={"10144"}>{"<< "  + id + ": $5,500"}</Link> <br/>
                </>
            }
                childrenBody={
                <>
           <ThemeProvider theme={theme}>
               <Stack direction="column" spacing={1.5} className={"mx-4 my-3"}>
                   <Button onClick={(x) => console.log(x)} variant="contained" href="#contained-buttons" disableElevation color={"primary"}>
                    pay now
                </Button>
            </Stack>
            </ThemeProvider>
                </>

            }
            />

            </>
    )
}