"use client";
import PrimaryText from "../../components/PrimaryText";
import Link from "next/link";
import {Button, Container, Divider} from "@mui/material";
const myFont = localFont({ src: 'GoldmanSans_Th.ttf' });
import Grid from "@mui/material/Unstable_Grid2"
import DeleteIcon from '@mui/icons-material/Delete';
import IconButton from '@mui/material/IconButton';
import Stack from '@mui/material/Stack';
import AdminPanelSettingsIcon from '@mui/icons-material/AdminPanelSettings';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { grey } from '@mui/material/colors';
import localFont from "@next/font/local";


import { styled } from '@mui/material/styles';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import Typography from '@mui/material/Typography';

import PropTypes from 'prop-types';
import Slider, { SliderThumb } from '@mui/material/Slider';
import Tooltip from '@mui/material/Tooltip';
import Box from '@mui/material/Box';
import React from 'react';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';


const theme = createTheme({
    palette: {
        text: {
            primary: "#000000",
            secondary: "#000000"
        },
        primary: {
            main: grey["A100"],
        },
    },

    typography: {
        fontFamily: "var(--font-inter)",
    }
});

const themeMenu = createTheme({
        palette: {
            text: {
                primary: "#FFFFFF",
                secondary: "#FFFFF"
            },
            primary: {
                main: grey["A100"],
            },
            background: {
                paper: "#000000"
            }
        },


        typography: {
            fontFamily: "var(--font-inter)",
        }


});
export default function Page(){
    return(

            <>

            <PrimaryText childrenMenu={
                <>
                <Link href="/" alt={"10144"}>{"<< autopay: on"}</Link> <br/>
                </>


            }
            childrenBody={
                <>
                {(true) ? (<AutopaySettings/>):(<ContainedButtons/>)}
                </>
            }
            />




            </>
    )
}

function AutopaySettings(){
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

        const [anchorEl2, setAnchorEl2] = React.useState(null);
        const open2 = Boolean(anchorEl2);
        const handleClickUsers = (event) => {
            setAnchorEl2(event.currentTarget);
        };
        const handleClose2 = () => {
            setAnchorEl2(null);
        };
    return(
            <>

            <ThemeProvider theme={themeMenu}>

            <Menu
                id="demo-positioned-menu"
                aria-labelledby="demo-positioned-button"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
                >
                <MenuItem onClick={handleClose}>Chase Checking 1</MenuItem>
                <MenuItem onClick={handleClose}>Cash App Checking</MenuItem>
                <MenuItem onClick={handleClose}>Goldman Sachs Bank</MenuItem>
            </Menu>

                <Menu
                    id="demo-positioned-menu"
                    aria-labelledby="demo-positioned-button"
                    anchorEl={anchorEl2}
                    open={open2}
                    onClose={handleClose2}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'left',
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'left',
                    }}
                    >
                    <MenuItem> <>  <p> cyberneticstream@gmail.com </p>  <IconButton onClick={(x)=> console.log(x)}> </IconButton> <DeleteOutlineIcon style={{ color: "#FFFFFF" }}/> </> </MenuItem>
                    <MenuItem onClick={handleClose2}>cyberneticstream@icloud.com</MenuItem>
                    <MenuItem onClick={handleClose2}>samuelbegie21@gmail.com</MenuItem>
                    <MenuItem onClick={handleClose2}>+ email </MenuItem>

                </Menu>
            </ThemeProvider>
            <ThemeProvider theme={theme}>


            <Box container sx={{width:"100%"}} className={"mx-auto "}>
            <Grid container>
                <Grid className={""}  sx = {{width:"50%" }}>
                    <Grid className={" relative  left-[28%] "} sx = {{width:50, height:36 }}>

            <FormGroup><Stack direction ={"row"}>
                <FormControlLabel control={ <IOSSwitch className={"my-1"} sx={{ m: 1 }} defaultChecked size = "large"/>} label={<><Tooltip title={"autopay setting"}><p>autopay</p></Tooltip></>}/>

             </Stack>
            </FormGroup>
                    </Grid>
                </Grid>
                <Grid className={""} sx = {{width:"50%" }}>
                    <Grid className={" w-fit fixed right-[14%]"}  >
                        <Button id="demo-positioned-button" aria-controls={open ? 'demo-positioned-menu' : undefined} aria-haspopup="true" onClick={handleClick} variant="contained" href="#contained-buttons" disableElevation color={"primary"}>
                            link bank </Button>
                    </Grid>
                </Grid>

                <Grid sx = {{width:500 }}className={"mx-auto "}>

    <Box sx={{ width: "73%" }} className={"mt-8 mx-auto "}  >
        <Stack direction={"row"} spacing={1.5}>
        <IOSSlider
                        aria-label="slider"
                        min = {-15}
                        max = {5}
                        marks={marks}
                        step={1}
                        valueLabelDisplay="on"
                    />
        <Tooltip title={"for autopay and without"} placement={"right-start"} >
        <p className={"pt-1"}>timing</p>
        </Tooltip>
            </Stack>
    </Box>
                </Grid>
            </Grid>
                </Box>
            <Box className={"fixed bottom-0 left-1/2 -translate-x-1/2"}>
            <Tooltip title="users" className={""}>
                <IconButton onClick={handleClickUsers}>
                    <AdminPanelSettingsIcon />
                </IconButton>
            </Tooltip>
            </Box>
            </ThemeProvider>

    </>
    )
}

function ContainedButtons() {
    return (
            <ThemeProvider theme={theme}>
            <Stack direction="column" spacing={1.5} className={"mx-4 my-3"}>
                <Button onClick={(x) => console.log(x)} variant="contained" href="#contained-buttons" disableElevation color={"primary"}>
                    cyberneticstream@gmail.com
                </Button>
                <Button onClick={(x) => console.log(x)} variant="contained" href="#contained-buttons" disableElevation color={"primary"}>
                    cyberneticstream@icloud.com
                </Button>
                <Button onClick={(x) => console.log(x)} variant="contained" href="#contained-buttons" disableElevation color={"primary"}>
                    samuelbegie21@gmail.com
                </Button>
            </Stack>
            </ThemeProvider>
            );
}

const IOSSwitch = styled((props) => (
        <Switch focusVisibleClassName=".Mui-focusVisible" disableRipple {...props} />
        ))(({ theme }) => ({
    width: 42,
    height: 26,
    padding: 0,
    '& .MuiSwitch-switchBase': {
        padding: 0,
        margin: 2,
        transitionDuration: '300ms',
        '&.Mui-checked': {
            transform: 'translateX(16px)',
            color: '#fff',
            '& + .MuiSwitch-track': {
                backgroundColor: theme.palette.mode === 'dark' ? '#2ECA45' : '#000000',
                opacity: 1,
                border: 0,
            },
            '&.Mui-disabled + .MuiSwitch-track': {
                opacity: 0.5,
            },
        },
        '&.Mui-focusVisible .MuiSwitch-thumb': {
            color: '#33cf4d',
            border: '6px solid #fff',
        },
        '&.Mui-disabled .MuiSwitch-thumb': {
            color:
            theme.palette.mode === 'light'
            ? theme.palette.grey[100]
            : theme.palette.grey[600],
        },
        '&.Mui-disabled + .MuiSwitch-track': {
            opacity: theme.palette.mode === 'light' ? 0.7 : 0.3,
        },
    },
    '& .MuiSwitch-thumb': {
        boxSizing: 'border-box',
        width: 22,
        height: 22,
    },
    '& .MuiSwitch-track': {
        borderRadius: 26 / 2,
        backgroundColor: theme.palette.mode === 'light' ? '#E9E9EA' : '#39393D',
        opacity: 1,
        transition: theme.transitions.create(['background-color'], {
            duration: 500,
        }),
    },
}));






const iOSBoxShadow =
'0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.13),0 0 0 1px rgba(0,0,0,0.02)';

const marks = [
    {
        value: -15,
        label: -15
    },
    {
        value: 5,
        label: 5

    }
    ];

const IOSSlider = styled(Slider)(({ theme }) => ({
    color: theme.palette.mode === 'dark' ? '#3880ff' : '#000000',
    height: 2,
    padding: '15px 0',
    '& .MuiSlider-thumb': {
        height: 28,
        width: 28,
        backgroundColor: '#fff',
        boxShadow: iOSBoxShadow,
        '&:focus, &:hover, &.Mui-active': {
            boxShadow:
            '0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.3),0 0 0 1px rgba(0,0,0,0.02)',
            // Reset on touch devices, it doesn't add specificity
            '@media (hover: none)': {
                boxShadow: iOSBoxShadow,
            },
        },
    },
    '& .MuiSlider-valueLabel': {
        fontSize: 12,
        fontWeight: 'normal',
        top: -6,
        backgroundColor: 'unset',
        color: theme.palette.text.primary,
        '&:before': {
            display: 'none',
        },
        '& *': {
            background: 'transparent',
            color: theme.palette.mode === 'dark' ? '#fff' : '#000',
        },
    },
    '& .MuiSlider-track': {
        border: 'none',
    },
    '& .MuiSlider-rail': {
        opacity: 0.5,
        backgroundColor: '#000000',
    },
    '& .MuiSlider-mark': {
        backgroundColor: '#000000',
        height: 8,
        width: 1,
        '&.MuiSlider-markActive': {
            opacity: 1,
            backgroundColor: 'currentColor',
        },
    },
}));